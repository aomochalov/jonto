package com.aomochalov.model;

import java.util.ArrayList;
import java.util.List;

public class Class {
    private String name;
    private List<Class> subClasses;
    private List<Property> propertyList;

    public Class(String name) {
        this.name = name;
        subClasses = new ArrayList<>();
        propertyList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Class> getSubClasses() {
        return subClasses;
    }

    public List<Property> getPropertyList() {
        return propertyList;
    }

    public String toString() {
        StringBuilder result = new StringBuilder("{ Class " + name);
        for (Property property : propertyList) {
            result.append(" has property ").append(property);
        }
        for (Class c : subClasses) {
            result.append(" has subClass ").append(c);
        }
        result.append(" }\n");
        return result.toString();
    }
}
