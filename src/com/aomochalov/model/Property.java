package com.aomochalov.model;

import java.util.ArrayList;
import java.util.List;

public class Property {
    private String name;
    private List<Property> subProperty;

    public Property(String name) {
        this.name = name;
        subProperty = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Property> getSubProperty() {
        return subProperty;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(name + "\n");
        for (Property p : subProperty) {
            result.append(" has subProperty ").append(p);
        }

        return result.toString();
    }
}
