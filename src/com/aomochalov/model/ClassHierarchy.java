package com.aomochalov.model;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.util.iterator.ExtendedIterator;

import java.util.*;

public class ClassHierarchy {
    private List<Class> classes = new ArrayList<>();

    public void showHierarchy(OntModel m) {
        // create an iterator over the root classes that are not anonymous class expressions
        Iterator<OntClass> i = m.listClasses();

        while (i.hasNext()) {
            OntClass ontClass = (OntClass) i.next();
            // Add root classes in our list
            if (ontClass.isURIResource()) {
                classes.add(new Class(ontClass.getLocalName()));
                showClass(ontClass);
            }
        }

        for (Class c : classes) {
            System.out.println(c);
        }
    }

    protected void showClass(OntClass cls) {
        renderClassDescription(cls);

        // recurse to the next level down
        if (cls.canAs(OntClass.class)) {
            for (Iterator<OntClass> i = cls.listSubClasses(true); i.hasNext(); ) {
                OntClass sub = i.next();
                // Add subclasses in root class
                classes.get(classes.size() - 1).getSubClasses().add(new Class(sub.getLocalName()));
                showClass(sub);
            }
        }
    }

    public void renderClassDescription(OntClass c) {
        ExtendedIterator<OntProperty> listDeclaredProperties = c.listDeclaredProperties();
        while (listDeclaredProperties.hasNext()) {
            OntProperty property = listDeclaredProperties.next();
            classes.get(classes.size() - 1).getPropertyList().add(new Property(property.getLocalName()));
            //addProperty(property, classes.get(classes.size() - 1).getPropertyList().get(classes.get(classes.size()
            // - 1).getPropertyList().size() -1));
        }
    }

    private void addProperty(OntProperty ontProperty, Property property) {
        ExtendedIterator<? extends OntProperty> ontProperties = ontProperty.listSubProperties(true);
        while (ontProperties.hasNext()) {
            OntProperty ontProp = ontProperties.next();
            property.getSubProperty().add(new Property(ontProp.getLocalName()));
            addProperty(ontProp, property);
        }
    }
}
