package com.aomochalov.ontology;

import com.aomochalov.model.ClassHierarchy;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;

public class Onto {
    public static void main(String[] args) {
        OntModel model = ModelFactory.createOntologyModel();
        model.read("file:rdfxml.owl");

        new ClassHierarchy().showHierarchy(model);
    }
}
